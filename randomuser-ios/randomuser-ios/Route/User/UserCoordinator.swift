//
//  RootCoordinator.swift
//  randomuser-ios
//
//  Created by AGM Tazim on 31/7/21.
//

import UIKit

class UserCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    public func start() {
        let repository = UserRepository(apiClient: APIClient.shared)
        let usecase = UserUsecase(repository: repository)
        let viewModel = UserViewModel(usecase: usecase)
        let vc = UserListViewController(viewModel: viewModel)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func showDetailsController(user: User) {
        let repository = UserRepository(apiClient: APIClient.shared)
        let usecase = UserUsecase(repository: repository)
        let viewModel = UserViewModel(usecase: usecase)
        let vc = DetailsViewController.instantiate(viewModel: viewModel)
        vc.user = user
        self.navigationController.pushViewController(vc, animated: true)
    }
}
