//
//  MoviewRequestApi.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import Foundation

enum UserApiRequest {
    case getUser(params: Parameterizable)
}

extension UserApiRequest: APIRequest {
    public var baseURL: URL {
        let url =  "\(AppConfig.shared.getServerConfig().getBaseUrl())/\(AppConfig.shared.getServerConfig().getApiVersion())/"
        return URL(string: url)!
    }
    
    public typealias ItemType = User
    public typealias ResponseType = Response<ItemType>
    
    public var method: RequestType {
        switch self {
            case .getUser: return .GET
        }
    }
    
    public var path: String {
        switch self {
            case .getUser: return ""
        }
    }
    
    public var parameters: [String: Any]{
        var parameter: [String: Any] = [:]
        
        switch self {
            case .getUser (let params):
                parameter = params.asRequestParam
        }
        
        return parameter
    }
    
    public var headers: [String: String] {
        return [String: String]()
    }
}


