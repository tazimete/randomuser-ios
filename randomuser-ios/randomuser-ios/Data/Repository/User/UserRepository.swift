//
//  MovieRepository.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import Foundation
import RxSwift

/* This is User repository class implementation from AbstractUserRepository. Which will be used to get user related from api client/server response*/
class UserRepository: AbstractUserRepository {
    var apiClient: AbstractApiClient
    
    init(apiClient: AbstractApiClient = APIClient.shared) {
        self.apiClient = apiClient
    }
    
    public func get(limit: Int) -> Observable<UserApiRequest.ResponseType> {
        return apiClient.send(apiRequest: UserApiRequest.getUser(params: UserParams(limit: limit)), type: UserApiRequest.ResponseType.self)
    }
}
