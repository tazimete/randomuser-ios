//
//  Entity.swift
//  setScheduleTest
//
//  Created by JMC on 31/10/21.
//

import Foundation

/* Wrapper response of all api, which has array of dynamic item like - user */
public struct Response<T: Codable>: Codable {
    public let results: [T]?
    
    public init(results: [T]? = nil) {
        self.results = results
    }
    
    public enum CodingKeys: String, CodingKey {
        case results = "results"
    }
}
