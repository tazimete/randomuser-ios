//
//  User.swift
//  randomuser-ios
//
//  Created by AGM Tazim on 31/10/21.
//

import Foundation

/* User entity of api response */
struct User: Codable {
    public let gender: String?
    public let name: Name?
    public let location: Location?
    public let email: String?
    public let login: Login?
//    public let phone: Int? // commented for inconsistent data type comes from server
    public let picture: Picture?
    
    init(gender: String? = nil, name: Name? = nil, location: Location? = nil, email: String? = nil, login: Login? = nil, picture: Picture? = nil) {
        self.gender = gender
        self.name = name
        self.location = location
        self.email = email
        self.login = login
//        self.phone = phone
        self.picture = picture
    }
    
    enum CodingKeys: String, CodingKey {
        case gender = "gender"
        case name = "name"
        case location = "location"
        case email = "email"
        case login = "login"
//        case phone = "phone" // commented for inconsistent data type comes from server
        case picture = "picture"
    }
    
    public var asCellViewModel: AbstractCellViewModel {
        return UserCellViewModel(id: login?.uuid, thumbnail: picture?.medium, title: "\(name?.title ?? "") \(name?.first ?? "") \(name?.last ?? "")", overview: location?.country)
    }
}

struct Name: Codable {
    public let title: String?
    public let first: String?
    public let last: String?
    
    init(title: String? = nil, first: String? = nil, last: String? = nil) {
        self.title = title
        self.first = first
        self.last = last
    }
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case first = "first"
        case last = "last"
    }
}

struct Login: Codable {
    public let uuid: String?
    public let username: String?
    public let password: String?
    
    init(uuid: String? = nil, username: String? = nil, password: String? = nil) {
        self.uuid = uuid
        self.username = username
        self.password = password
    }
    
    enum CodingKeys: String, CodingKey {
        case uuid = "uuid"
        case username = "username"
        case password = "password"
    }
}

struct Location: Codable {
    public let street: Street?
    public let city: String?
    public let state: String?
//    public let postcode: Int? // commented for inconsistent data type comes from server
    public let country: String?
    
    init(street: Street? = nil, city: String? = nil, state: String? = nil, country: String?) {
        self.street = street
        self.city = city
        self.state = state
//        self.postcode = postcode // commented for inconsistent data type comes from server
        self.country = country
    }
    
    enum CodingKeys: String, CodingKey {
        case street = "street"
        case city = "city"
        case state = "state"
//        case postcode = "postcode" // commented for inconsistent data type comes from server
        case country = "country"
    }
}

struct Street: Codable {
    public let name: String?
//    public let number: Int? // commented for inconsistent data type comes from server
    
    init(name: String? = nil, number: Int? = nil) {
        self.name = name
//        self.number = number // commented for inconsistent data type comes from server
    }
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
//        case number = "number" // commented for inconsistent data type comes from server
    }
}

struct Picture: Codable {
    public let large: String?
    public let medium: String?
    public let thumbnail: String?
    
    init(large: String? = nil, medium: String? = nil, thumbnail: String? = nil) {
        self.large = large
        self.medium = medium
        self.thumbnail = thumbnail
    }
    
    enum CodingKeys: String, CodingKey {
        case large = "large"
        case medium = "medium"
        case thumbnail = "thumbnail"
    }
}
