//
//  AbstractUserViewModel.swift
//  randomuser-ios
//
//  Created by AGM Tazim on 2/11/21.
//

import Foundation
import RxSwift

/* This is User viewmodel abstraction extented from AbstractViewModel. Which will be used to get user related data by user usecase*/
protocol AbstractUserViewModel: AbstractViewModel {
    // Transform the user list input to output observable
    func getUserListOutput(input: UserViewModel.UserListInput) -> UserViewModel.UserListOutput
//    func getUserDetailsOutput(input: UserViewModel.UserInput) -> UserViewModel.UserOutput
    
    // get user data through api call
    func getUserList(limit: Int) -> Observable<UserApiRequest.ResponseType>
//    func getUserDetails(userId: Int) -> Observable<UserApiRequest.ResponseType>
}

