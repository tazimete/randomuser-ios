//
//  ViewController.swift
//  setScheduleTest
//
//  Created by JMC on 29/10/21.
//

import UIKit
import RxSwift
import RxCocoa

class UserListViewController: BaseViewController {
    // MARK: Non UI Proeprties
    public var userViewModel: AbstractUserViewModel!
    private let disposeBag = DisposeBag()
    private let userListTrigger = PublishSubject<UserViewModel.UserListInputModel>()
    private var rootCoordinator: Coordinator?
    
    // MARK: UI Proeprties
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        //cell registration
        collectionView.register(UserItemCell.self, forCellWithReuseIdentifier: UserItemCellConfig.reuseId)
        collectionView.register(UserShimmerCell.self, forCellWithReuseIdentifier: ShimmerItemCellConfig.reuseId)
        
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        return collectionView
    }()
    
    let lblNoData: UILabel = {
        let label = UILabel()
        label.text = "No data to show, Plz search by tapping on search button in top bar."
        label.textColor = .darkGray
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.isSkeletonable = false
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()

    // MARK: Constructors
    init(viewModel: AbstractUserViewModel) {
        super.init(viewModel: viewModel)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        rootCoordinator = (view.window?.windowScene?.delegate as! SceneDelegate).rootCoordinator
    }

    // MARK: Overrriden Methods
    override func initView() {
        super.initView()
        
        //setup tableview
        view.addSubview(collectionView)
//        view.addSubview(lblNoData)
        
        //set anchor
        collectionView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: view.frame.width, height: 0, enableInsets: true)
//        lblNoData.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: view.frame.width, height: 0, enableInsets: true)
        
        //table view
        onTapTableviewCell()
    }
    
    //when theme change, we can also define dark mode color option in color asse
    override public func applyDarkTheme() {
        navigationController?.navigationBar.backgroundColor = .lightGray
        collectionView.backgroundColor = .black
        self.navigationItem.rightBarButtonItem?.tintColor = .lightGray
        collectionView.reloadData()
    }
    
    override public func applyNormalTheme() {
        navigationController?.navigationBar.backgroundColor = .white
        collectionView.backgroundColor = .white
        self.navigationItem.rightBarButtonItem?.tintColor = .black
        collectionView.reloadData()
    }
    
    override func initNavigationBar() {
        self.navigationItem.title = "User List"
    }
    
    override func bindViewModel() {
        userViewModel = (viewModel as! AbstractUserViewModel)
        let input = UserViewModel.UserListInput(trigger: userListTrigger)
        let output = userViewModel.getUserListOutput(input: input)

        //populate table view
        output.userList.observe(on: MainScheduler.instance)
            .bind(to: collectionView.rx.items) { [weak self] collectionView, row, model in
                guard let weakSelf = self else {
                    return UICollectionViewCell()
                }

                return weakSelf.populateTableViewCell(viewModel: model.asCellViewModel, indexPath: IndexPath(row: row, section: 0), collectionView: collectionView)
            }.disposed(by: disposeBag)

        // detect error
        output.errorTracker.observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in

                guard let weakSelf = self, let error = error else {
                    return
                }

            print("\(String(describing: weakSelf.TAG)) -- bindViewModel() -- error  -- code = \(error.errorCode), message = \(error.errorMessage)")
        }).disposed(by: disposeBag)
        
        // fire user list trigger
        getUserList(limit: 10)
    }
    
    public func getUserList(limit: Int) {
        hideNoDataMessageView()
        userListTrigger.onNext(UserViewModel.UserListInputModel(limit: limit))
    }
    
    private func hideNoDataMessageView() {
        lblNoData.isHidden = true
    }
    
    private func navigateToDetailsController(with user: User) {
        rootCoordinator?.showDetailsController(user: user)
    }
    
    //populate table view cell
    private func populateTableViewCell(viewModel: AbstractCellViewModel, indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell {
        var item: CellConfigurator = ShimmerItemCellConfig.init(item: UserCellViewModel())
        
        // check actual data exists or not, to hide shimmer cell
        if viewModel.id != nil {
            item = UserItemCellConfig.init(item: viewModel)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        return cell
    }
    
    // MARK: Actions
    private func onTapTableviewCell() {
        Observable
            .zip(collectionView.rx.itemSelected, collectionView.rx.modelSelected(User.self))
            .bind { [weak self] indexPath, model in
                guard let weakSelf = self else {
                    return
                }
                
                weakSelf.collectionView.deselectItem(at: indexPath, animated: true)
                print("\(weakSelf.TAG) -- onTapTableviewCell() -- Selected " + (model.name?.first ?? "") + " at \(indexPath)")
                
                //navigate to profile view controller
                weakSelf.navigateToDetailsController(with: model)
            }
            .disposed(by: disposeBag)
    }
}


extension UserListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: (collectionView.frame.width/3)-15, height: 200)
    }
}
