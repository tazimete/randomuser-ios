//
//  DetailsViewController.swift
//  setScheduleTest
//
//  Created by JMC on 1/11/21.
//

import UIKit
import RxSwift
import RxCocoa

class DetailsViewController: BaseViewController {
    // MARK: Non UI Objects
    public var user: User!
    private let disposeBag = DisposeBag()
    
    // MARK: UI Objects
    @IBOutlet weak var ivProfilePicture: UIImageView!
    @IBOutlet weak var uivInfoContainer: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lvlAddress: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindData(data: user)
    }

    override func initView() {
        super.initView()
        ivProfilePicture.contentMode = .scaleAspectFill
        
        //start shimmer animation
        startShimmerAnimation()
        
        // add gesture
        lblEmail.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer()
        lblEmail.addGestureRecognizer(tapGesture)
        
        tapGesture.rx.event.bind(onNext: { [weak self] recognizer in
            print("touches: \(recognizer.numberOfTouches)") //or whatever you like
            self?.openEmail(email: self?.user.email ?? "")
        }).disposed(by: disposeBag)
    }
    
    override func initNavigationBar() {
        self.navigationItem.title = "Details"
    }
    
    private func bindData(data: User) {
        let avatarUrl = "\(AppConfig.shared.getServerConfig().getMediaBaseUrl())\(data.picture?.large ?? "")"
        
        ivProfilePicture.loadImage(from: avatarUrl, completionHandler: {
            [weak self] url, image, isFinished in
            
            guard let weakSelf = self else {
                return
            }
            
            weakSelf.ivProfilePicture.image = image?.decodedImage(size: weakSelf.ivProfilePicture.bounds.size)
            weakSelf.stopShimmerAnimation(views: [weakSelf.ivProfilePicture])
        })
        
        lblName.text = "Name: \(data.name?.title ?? "") \(data.name?.first ?? "") \(data.name?.last ?? "")"
        lblEmail.text = "Email : \(data.email ?? "")"
        lvlAddress.text = "Address : \(data.location?.street?.name ?? ""), \(data.location?.city ?? "")"
        lblCountry.text = "Country : \(data.location?.state ?? ""), \(data.location?.country ?? "")"
        
        //stop shimmer
        stopShimmerAnimation()
    }
    
    private func startShimmerAnimation() -> Void {
       //shmmer skeleton animation
        startShimmerAnimation(views: [ivProfilePicture, lblName, lblEmail, uivInfoContainer])
   }
       
   //stop shimmer animation
   private func stopShimmerAnimation() -> Void {
        stopShimmerAnimation(views: [uivInfoContainer, lblName, lblEmail])
   }
    
    private func openEmail(email: String) {
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
}
