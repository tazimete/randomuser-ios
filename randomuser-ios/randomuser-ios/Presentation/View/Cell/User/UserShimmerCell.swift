//
//  ShimmerCell.swift
//  rabdomuse-ios
//
//  Created by AGM Tazim on 30/10/21.
//

import UIKit

class UserShimmerCell: UserItemCell {
    typealias DataType = AbstractCellViewModel
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func configure(data: DataType) {
        applyTheme()
        containerView.layer.borderWidth = 0
        containerView.layer.borderColor = UIColor.clear.cgColor
        
        //shmmer skeleton animation
        ShimmerHelper.startShimmerAnimation(viewlist: [lblTitle, lblOverview, ivAvatar])
    }
    
    // when theme change (dark or normal)
    override public func applyTheme() {
        switch (traitCollection.userInterfaceStyle) {
            case .dark:
                containerView.backgroundColor = .black
                containerView.layer.borderColor = UIColor.white.cgColor
                backgroundView?.backgroundColor = .black
                break

            case .light:
                containerView.backgroundColor = .white
                containerView.layer.borderColor = UIColor.black.cgColor
                break

            default:
                break
        }
    }
}
